PRODUCT_COPY_FILES += \
vendor/gavapps/Permissions/privapp-permissions-google-all.xml:system/etc/permissions/privapp-permissions-google-all.xml \
vendor/gavapps/Permissions/opengapps-default-permissions.xml:system/etc/permissions/opengapps-default-permissions.xml

PRODUCT_PACKAGES += \
    YouTube \
    Drive \
    GoogleDialer \
    GCS \
    GoogleOneTimeInitializer \
    WellbeingPrebuilt \
    GoogleCamera \
    GoogleContacts \
    CalendarGooglePrebuilt \
    CalculatorGooglePrebuilt \
    Photos \
    PrebuiltBugle \
    PrebuiltDeskClockGoogle
